package com.example.demo.service;

import com.example.demo.dto.BorrowerDto;
import com.example.demo.model.Borrower;
import com.example.demo.repository.BorrowerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BorrowerServiceTest {

    @Mock
    BorrowerRepository borrowerRepositoryMock;

    @InjectMocks
    BorrowerService borrowerService;

    @Before
    public void setUp() {
        List<Borrower> borrowers = Arrays.asList(
                new Borrower("user a"),
                new Borrower("user b")
        );
        when(borrowerRepositoryMock.findAll()).thenReturn(borrowers);
    }

    @Test
    public void testFindByName_whenFoundOne() {
        List<BorrowerDto> actual = borrowerService.findByName("a");
        List<BorrowerDto> expected = Arrays.asList(new BorrowerDto("user a"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFindByName_whenNotFound() {
        List<BorrowerDto> actual = borrowerService.findByName("xyz");
        List<BorrowerDto> expected = Collections.emptyList();
        Assert.assertEquals(expected, actual);
    }
}
