package com.example.demo.service;

import com.example.demo.model.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

class RuleServiceTest {

    private Instant REGISRATION_TIME = Instant.now().minus(90, ChronoUnit.DAYS);

    RuleService ruleService = new RuleService(7, 7, 28, 5, 90);

    @Test
    void calculateAvailableUntil_whenCommon() {
        Book book = getBook(100, REGISRATION_TIME);
        LocalDate calculates = ruleService.calculateAvailableUntil(book);

        LocalDate expected = LocalDate.now().plusWeeks(4);
        Assertions.assertEquals(expected, calculates);
    }

    @Test
    void calculateAvailableUntil_whenNew() {
        Book book = getBook(100, Instant.now().minus(7, ChronoUnit.DAYS));
        LocalDate calculates = ruleService.calculateAvailableUntil(book);

        LocalDate expected = LocalDate.now().plusWeeks(1);
        Assertions.assertEquals(expected, calculates);
    }

    @Test
    void calculateAvailableUntil_whenFew() {
        Book book = getBook(4, REGISRATION_TIME);
        LocalDate calculates = ruleService.calculateAvailableUntil(book);

        LocalDate expected = LocalDate.now().plusWeeks(1);
        Assertions.assertEquals(expected, calculates);
    }

    private Book getBook(int copies, Instant registrationTime) {
        return new Book(1L, "Title", "Location:A", copies, registrationTime, 0);
    }
}
