package com.example.demo.controller;

import com.example.demo.dto.BorrowerDto;
import com.example.demo.service.BorrowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/borrowers")
public class BorrowerController {

    @Autowired
    private BorrowerService borrowerService;

    @GetMapping("/find/{namePart}")
    public List<BorrowerDto> getAllBorrowers(@PathVariable String namePart) {
        return borrowerService.findByName(namePart);
    }

    @PostMapping
    public BorrowerDto addBorrower(@RequestBody BorrowerDto borrowerDto) {
        borrowerService.addBorrower(borrowerDto);
        return borrowerDto;
    }

    @DeleteMapping
    public BorrowerDto deleteBorrower(@RequestBody BorrowerDto borrowerDto) {
        borrowerService.deleteBorrower(borrowerDto);
        return borrowerDto;
    }
}
