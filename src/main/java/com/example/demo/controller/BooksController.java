package com.example.demo.controller;

import com.example.demo.dto.BookDto;
import com.example.demo.dto.LoanDto;
import com.example.demo.dto.LoanReportDto;
import com.example.demo.service.BookService;
import com.example.demo.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin  //TODO
@RequestMapping("/api")
public class BooksController {

    @Autowired
    private BookService bookService;

    @Autowired
    private LoanService loanService;

    @PostMapping("/books")
    public BookDto addBook(@RequestBody BookDto bookDto) {
        bookService.addBook(bookDto);
        return bookDto;
    }

    @GetMapping("/books")
    public List<BookDto> getAllBooks() {
        return bookService.listAllBooks();
    }

    @GetMapping("/books/over/report")
    public List<LoanReportDto> getAllOverReport() {
        return bookService.getAllOverReport();
    }

    @GetMapping("/books/find/{titlePart}")
    public List<BookDto> findBook(@PathVariable String titlePart) {
        return bookService.findBookByName(titlePart);
    }

    @PostMapping("/books/loan")
    public LoanDto loanBook(@RequestBody LoanDto loanDto) {
        loanService.register(loanDto);
        return loanDto;
    }

    @DeleteMapping("/books/loan")
    public LoanDto refundBook(@RequestBody LoanDto loanDto) {
        loanService.unRegister(loanDto);
        return loanDto;
    }
}
