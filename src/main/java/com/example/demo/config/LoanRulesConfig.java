package com.example.demo.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class LoanRulesConfig {
    @Value("${librarian.loan.days.few:7}")
    private int daysFew;
    @Value("${librarian.loan.days.new:7}")
    private int daysNew;
    @Value("${librarian.loan.days.general:28}")
    private int daysGeneral;
    @Value("${librarian.loan.days.few.is.copies:5}")
    private int fewIsCopies;
    @Value("${librarian.loan.days.new.is.days:90}")
    private int newIsDays;
}
