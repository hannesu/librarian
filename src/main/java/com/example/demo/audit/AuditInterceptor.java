package com.example.demo.audit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Slf4j
public class AuditInterceptor extends HandlerInterceptorAdapter
{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        log.info("AUDIT: REQUEST: {} {} {} {}",
                request.getMethod(),
                request.getServletPath() ,
                Optional.ofNullable(request.getRemoteUser()).orElse("-"),
                request.getSession().getId());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {
        log.info("AUDIT: RESPONSE: {} {} {} {}",
                request.getMethod(),
                request.getServletPath() ,
                response.getStatus(),
                request.getSession().getId());
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception
    {
        log.info("AUDIT: COMPLETED: {} {} {}",
                request.getMethod(),
                request.getServletPath() ,
                request.getSession().getId());
    }
}
