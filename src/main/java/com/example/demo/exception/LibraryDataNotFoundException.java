package com.example.demo.exception;

public class LibraryDataNotFoundException extends RuntimeException {
    public LibraryDataNotFoundException(String message) {
        super(message);
    }
}
