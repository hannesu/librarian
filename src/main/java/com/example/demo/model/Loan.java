package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor
public class Loan {
    private long bookId;
    private String borrowerName;
    private Instant returnDate;

    public Loan(long bookId, String borrowerName) {
        this.bookId = bookId;
        this.borrowerName = borrowerName;
    }
}
