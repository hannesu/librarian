package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor
public class Book {
    private final long bookId;
    private final String title;
    private final String location;
    private final int copies;
    private final Instant registrationDate;
    private final Integer borrows;

    public Book(long bookId, String title, String location, int copies, Instant registrationDate) {
        this.bookId = bookId;
        this.title = title;
        this.location = location;
        this.copies = copies;
        this.registrationDate = registrationDate;
        this.borrows = null;
    }
}
