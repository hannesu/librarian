package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

@Data
@AllArgsConstructor
public class LoanReport {
    private final Book book;
    private final Loan loan;

    public int getDaysOver() {
        int daysOver = Period.between(LocalDate.ofInstant(loan.getReturnDate(), ZoneId.systemDefault()), LocalDate.now()).getDays();
        return daysOver > 0 ? daysOver : 0;
    }
}
