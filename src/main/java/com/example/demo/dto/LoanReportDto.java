package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoanReportDto {
    private String title;
    private String borrowerName;
    private int daysOver;
}
