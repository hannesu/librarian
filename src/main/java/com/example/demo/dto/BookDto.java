package com.example.demo.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class BookDto {
    private long bookId;
    private String title;
    private String location;
    private int copies;
    private int availableCopies;
    private LocalDate registrationDate;
    private LocalDate availableUntilDate;
}
