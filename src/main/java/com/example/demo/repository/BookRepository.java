package com.example.demo.repository;

import com.example.demo.model.Book;
import com.example.demo.model.Loan;
import com.example.demo.model.LoanReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class BookRepository {

    public static final String SELECT_BOOKS_DETAILED = "select book.*, " +
            " (select count(loan.book_id) from loan where loan.book_id = book.book_id and loan.returned = false) as borrows " +
            " from book ";
    public static final String BOOKS_BY_ID = " where book_id = :bookId ";
    public static final String BOOKS_BY_TITLE = " where lower(title) like '%'||:title||'%' ";
    public static final String BOOKS_ORDER = " order by book.title;";

    public static final String SELECT_OVER_RETURN_DATE_LOANS = "select book.*, loan.* " +
            " from loan join book using(book_id) " +
            " where loan.returned = false " +
            " and loan.return_date < CURRENT_DATE() " +
            " order by loan.return_date desc, book.title;";

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<Book> findAll() {
        var sql = SELECT_BOOKS_DETAILED + BOOKS_ORDER;
        return namedParameterJdbcTemplate.query(sql, new RowMapperResultSetExtractor<Book>(getBookRowMapper()));
    }

    public  List<Book> findByTitle(String titlePart) {
        var sql = SELECT_BOOKS_DETAILED + BOOKS_BY_TITLE;
        return namedParameterJdbcTemplate.query(sql,
                new MapSqlParameterSource("title", titlePart.toLowerCase()),
                new RowMapperResultSetExtractor<Book>(getBookRowMapper()));
    }

    public List<Book> findById(long id) {
        var sql = SELECT_BOOKS_DETAILED + BOOKS_BY_ID;
        return namedParameterJdbcTemplate.query(sql,
                new MapSqlParameterSource("bookId", id),
                new RowMapperResultSetExtractor<Book>(getBookRowMapper()));
    }

    public List<LoanReport> getAllOverLoans() {
        return namedParameterJdbcTemplate.query(SELECT_OVER_RETURN_DATE_LOANS,
                new RowMapperResultSetExtractor<LoanReport>(getLoanReportRowMapper()));
    }

    private RowMapper<LoanReport> getLoanReportRowMapper() {
        return new RowMapper<LoanReport>() {
            @Override
            public LoanReport mapRow(ResultSet rs, int i) throws SQLException {
                var book = new Book(
                        rs.getLong("book_id"),
                        rs.getString("title"),
                        rs.getString("location"),
                        rs.getInt("copies"),
                        rs.getTimestamp("registration_date").toInstant());
                var loan = new Loan(
                        rs.getLong("book_id"),
                        rs.getString("borrower_name"),
                        rs.getTimestamp("return_date").toInstant()
                );
                return new LoanReport(book, loan);
            }
        };
    }

    public  int save(Book book) {
        return namedParameterJdbcTemplate.update("insert into book" +
                        " (book_id,  title,  location,  copies) " +
                        " values (seq_book_id.nextval,  :title,  :location,  :copies);",
                new MapSqlParameterSource()
                .addValue("title", book.getTitle())
                .addValue("location", book.getLocation())
                .addValue("copies", book.getRegistrationDate()));
    }

    private RowMapper<Book> getBookRowMapper() {
        return new RowMapper<Book>() {
            @Override
            public Book mapRow(ResultSet rs, int i) throws SQLException {
                return new Book(
                        rs.getLong("book_id"),
                        rs.getString("title"),
                        rs.getString("location"),
                        rs.getInt("copies"),
                        rs.getTimestamp("registration_date").toInstant(),
                        rs.getInt("borrows")
                );
            }
        };
    }
}

