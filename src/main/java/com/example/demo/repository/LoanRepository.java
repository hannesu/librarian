package com.example.demo.repository;

import com.example.demo.model.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

@Repository
public class LoanRepository {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public int register(Loan loan) throws SQLIntegrityConstraintViolationException {
        var paramMap =  new MapSqlParameterSource()
                .addValue("bookId", loan.getBookId())
                .addValue("borrowerName", loan.getBorrowerName())
                .addValue("returnDate", loan.getReturnDate());
        return namedParameterJdbcTemplate.update("insert into loan " +
                        " (book_id, borrower_name, return_date) " +
                        " values(:bookId, :borrowerName, :returnDate);", paramMap);
    }

    public int unregister(Loan loan) throws SQLException {
        return namedParameterJdbcTemplate.update("update loan set returned = true " +
                        " where book_id = :bookId and borrower_name = :borrowerName " +
                        " and returned = false " +
                        " limit 1;",
                new MapSqlParameterSource()
                .addValue("bookId", loan.getBookId())
                .addValue("borrowerName", loan.getBorrowerName())
        );
    }
}
