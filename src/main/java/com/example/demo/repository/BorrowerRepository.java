package com.example.demo.repository;

import com.example.demo.model.Borrower;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class BorrowerRepository {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<Borrower> findAll() {
        return namedParameterJdbcTemplate.query("select * from borrower", new MapSqlParameterSource(), new RowMapper<Borrower>() {
            @Override
            public Borrower mapRow(ResultSet rs, int i) throws SQLException {
                return new Borrower(rs.getString("name"));
            }
        });
    }

    public int save(Borrower borrower) throws SQLException {
        return namedParameterJdbcTemplate.update("insert into borrower " +
                        " (name) " +
                        " values(:name);",
                new MapSqlParameterSource("name", borrower.getName())
        );
    }

    public int deleteByName(String name) throws SQLException {
        return namedParameterJdbcTemplate.update("delete from borrower " +
                        " where name = :name;",
                new MapSqlParameterSource("name", name)
        );
    }
}
