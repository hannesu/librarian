package com.example.demo.service;

import com.example.demo.model.Book;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Service
public class RuleService {

    private int daysFew;
    private int daysNew;
    private int daysGeneral;
    private int fewIsCopies;
    private int newIsDays;

    public RuleService(
            @Value("${librarian.lend.days.few:7}") int daysFew,
            @Value("${librarian.lend.days.new:7}") int daysNew,
            @Value("${librarian.lend.days.general:28}") int daysGeneral,
            @Value("${librarian.lend.days.few.is.copies:5}") int fewIsCopies,
            @Value("${librarian.lend.days.new.is.days:7}") int newIsDays) {
        this.daysFew = daysFew;
        this.daysNew = daysNew;
        this.daysGeneral = daysGeneral;
        this.fewIsCopies = fewIsCopies;
        this.newIsDays = newIsDays;
    }

    public LocalDate calculateAvailableUntil(final Book  book) {
        if (book.getCopies() < fewIsCopies) {
            return LocalDate.now().plusDays(daysFew);
        }
        if (book.getRegistrationDate().plus(newIsDays, ChronoUnit.DAYS).isAfter(Instant.now())) {
            return LocalDate.now().plusDays(daysNew);
        }
        return LocalDate.now().plusDays(daysGeneral);
    }
}
