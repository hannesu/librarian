package com.example.demo.service;

import com.example.demo.dto.BookDto;
import com.example.demo.dto.LoanReportDto;
import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private RuleService ruleService;

    public List<BookDto> listAllBooks() {
        return bookRepository.findAll().stream()
                .map(book -> {
                    return BookDto.builder()
                            .bookId(book.getBookId())
                            .title(book.getTitle())
                            .location(book.getLocation())
                            .copies(book.getCopies())
                            .registrationDate(LocalDate.ofInstant(book.getRegistrationDate(), ZoneId.systemDefault()))
                            .availableUntilDate(ruleService.calculateAvailableUntil(book))
                            .availableCopies(calculateAvailableCopies(book))
                            .build();
                })
                .collect(Collectors.toList());
    }

    private int calculateAvailableCopies(Book book) {
        return book.getCopies() - book.getBorrows();
    }

    public List<BookDto> findBookByName(String titlePart) {
        return bookRepository.findByTitle(titlePart).stream()
                .map(book -> {
                    return BookDto.builder()
                            .bookId(book.getBookId())
                            .title(book.getTitle())
                            .location(book.getLocation())
                            .copies(book.getCopies())
                            .registrationDate(LocalDate.ofInstant(book.getRegistrationDate(), ZoneId.systemDefault()))
                            .availableUntilDate(ruleService.calculateAvailableUntil(book))
                            .availableCopies(calculateAvailableCopies(book))
                            .build();
                })
                .collect(Collectors.toList());
    }

    public void addBook(BookDto bookDto) {
        var mapper = new ModelMapper();
        var book = mapper.map(bookDto, Book.class);
        int result = bookRepository.save(book);
        Assert.state(result == 1, "Book adding failed!");
    }

    public List<LoanReportDto> getAllOverReport() {
        return bookRepository.getAllOverLoans().stream()
                .map(loanReport ->
                    new LoanReportDto(loanReport.getBook().getTitle(), loanReport.getLoan().getBorrowerName(), loanReport.getDaysOver())
                ).collect(Collectors.toList());
    }
}
