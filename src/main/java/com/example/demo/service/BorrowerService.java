package com.example.demo.service;

import com.example.demo.dto.BorrowerDto;
import com.example.demo.model.Borrower;
import com.example.demo.repository.BorrowerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.sql.SQLException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BorrowerService {

    @Autowired
    private BorrowerRepository borrowerRepository;

    public List<BorrowerDto> findByName(String namePart) {
        Predicate<Borrower> findPredicate = borrower -> borrower.getName().toLowerCase().contains(namePart);

        return borrowerRepository.findAll().stream()
                .filter(findPredicate)
                .map(borrower -> new BorrowerDto(borrower.getName()))
                .collect(Collectors.toList());
    }

    public void addBorrower(BorrowerDto borrower) {
        int result = 0;
        try {
            result = borrowerRepository.save(new Borrower(borrower.getName()));
        } catch (IllegalStateException e) {
            result = -1;
        } catch (SQLException e) {
            log.error("SQL error on borrower adding!", e);
        } finally {
            Assert.state(result == 1 || result == -1 , "Borrower already exists!");
            Assert.state(result == 1, "Borrower adding failed!");
        }
    }

    public void deleteBorrower(BorrowerDto borrowerDto) {
        int result = 0;
        try {
            result = borrowerRepository.deleteByName(borrowerDto.getName());
        } catch (Exception e) {
            Assert.state(result == 1, "As borrower has returned/not returned registrations deletion not allowed!");
        }
        Assert.state(result == 1 || result == -1 , "Borrower does not exists!");
        Assert.state(result == 1, "Borrower deleting failed!");
    }
}
