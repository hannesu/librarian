package com.example.demo.service;

import com.example.demo.dto.LoanDto;
import com.example.demo.model.Book;
import com.example.demo.model.Loan;
import com.example.demo.repository.BookRepository;
import com.example.demo.repository.LoanRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;

@Service
@Slf4j
public class LoanService {

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private RuleService ruleService;

    @Transactional
    public void register(LoanDto loanDto) {
        int result = 0;

        List<Book> found = bookRepository.findById(loanDto.getBookId());
        Assert.state(found.size() == 1, "Loan not registered, book does not exists!");
        var book = found.get(0);
        Instant returnDate = ruleService.calculateAvailableUntil(book).atStartOfDay(ZoneId.systemDefault()).toInstant();
        var loan = new Loan(loanDto.getBookId(), loanDto.getBorrowerName(), returnDate);

        try {
            result = loanRepository.register(loan);
        } catch (SQLIntegrityConstraintViolationException e) {
            Assert.state(result == 1, "Loan not registered, user does not exists!!");
        } finally {
            Assert.state(result == 1, "Loan not registered!");
        }
    }

    public void unRegister(LoanDto loanDto) {
        int result = 0;
        try {
            var loan = new Loan(loanDto.getBookId(), loanDto.getBorrowerName());
            result = loanRepository.unregister(loan);
        } catch (SQLException e) {
            throw new RuntimeException("Error on unregistering!", e);
        }
        Assert.state(result == 1 || result == -1 , "Loan does not exists!");
        Assert.state(result == 1, "Loan deleting failed!");
    }
}
