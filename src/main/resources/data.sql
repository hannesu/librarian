
insert into book(book_id, title, location, copies)
values(seq_book_id.nextval, 'KOSMOS. KÜSIMUSED JA VASTUSED MAAILMARUUMIST HUVILISELE LAPSELE', 'E1234567', 10);

insert into book(book_id, title, location, copies, registration_date)
values(seq_book_id.nextval, 'The Wild Nature', 'E1234567', 10, DATEADD('DAY', -30, CURRENT_TIMESTAMP));

insert into book(book_id, title, location, copies)
values(seq_book_id.nextval, 'LAEVAKOKK WEND', 'E1234567', 10);

insert into book(book_id, title, location, copies, registration_date)
values(seq_book_id.nextval, 'TÕDE JA ÕIGUS I', 'E1234567', 10, DATEADD('DAY', -91, CURRENT_TIMESTAMP));

insert into book(book_id, title, location, copies, registration_date)
values(seq_book_id.nextval, 'HARRY POTTER JA TARKADE KIVI', 'E1234567', 10, DATEADD('DAY', -91, CURRENT_TIMESTAMP));

insert into book(book_id, title, location, copies)
values(seq_book_id.nextval, 'JÄÄ JA LUMI', 'E1234567', 10);

insert into borrower values ('User A');
insert into borrower values ('User B');

insert into loan values(1,'User A', DATEADD('DAY', 7, CURRENT_TIMESTAMP), false);
insert into loan values(1,'User A', DATEADD('DAY', -1, CURRENT_TIMESTAMP), true);
insert into loan values(1,'User B', DATEADD('DAY', 7, CURRENT_TIMESTAMP), true);
insert into loan values(2,'User B', DATEADD('DAY', -7, CURRENT_TIMESTAMP), false);
insert into loan values(3,'User A', DATEADD('DAY', 7, CURRENT_TIMESTAMP), true);
