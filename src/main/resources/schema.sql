

create table book
(
   book_id integer not null,
   title varchar(255) not null,
   location varchar(255) not null,
   copies integer not null,
   registration_date timestamp default CURRENT_TIMESTAMP,
   primary key(book_id)
);

create sequence seq_book_id start 1;


create table borrower
(
    name varchar(255) not null,
    primary key(name)
);


create table loan
(
    book_id integer not null,
    borrower_name varchar(255) not null,
    return_date timestamp not null,
    returned boolean default false,
    foreign key (borrower_name) references borrower(name)
);
