#Librarian Demo Application

Exposing JSON API and Angular view

Tested platform: OpenJDK Runtime Environment (build 11.0.7+10-post-Ubuntu-2ubuntu218.04)

Database: H2 memory only instance
Maven:  12.16.2
Angular: 9.1.3
Node: 12.16.2
npm: 12.16.2


Application specific configuraton:
    librarian.loan.days.=7
    librarian.loan.days.new=7
    librarian.loan.days.general=28
    librarian.loan.days.few.is.copies=5
    librarian.loan.days.new.is.days=90

Usage:
git clone https://github.com/hannesu/librerian.git
mvn install
java -jar target/demo-0.0.1-SNAPSHOT.jar | mvn spring-boot:run 


##Requirements 
 * Specification (external)

 * Audit logging solutions demo:
 Here are shown different technical solutions for auditing - those can be used according needs and system specifics ... 
    -- By own WebMvcConfigurer Interceptor
            Output: stdout 
                INFO 22925 --- [nio-8080-exec-1] com.example.demo.audit.AuditInterceptor  : AUDIT: REQUEST: GET /api/books - F6533CD00D656E62310489557CF96598
                INFO 23712 --- [nio-8080-exec-1] com.example.demo.audit.AuditInterceptor  : AUDIT: RESPONSE: GET /api/books 200 21E029918B376280DAEED7B7495050F9
                INFO 23712 --- [nio-8080-exec-1] com.example.demo.audit.AuditInterceptor  : AUDIT: COMPLETED: GET /api/books 21E029918B376280DAEED7B7495050F9
  
    -- By using @Aspect @Around("execution(* com.example.demo.controller.*.*(..))")
            Output: stdout 
                INFO 23712 --- [nio-8080-exec-2] c.example.demo.audit.AuditLoggingAspect  : Execution time of BooksController.getAllBooks 0ms 
        - All possible pointcuts are: @Before, @After, @AfterReturning, @AfterThrowing, @Around   
         
    -- By HTTP trace via Spring Boot actuator management + com.example.demo.audit.AuditHttpTraceRepository
            Output:stdout
                INFO 23712 --- [nio-8080-exec-2] c.e.demo.audit.AuditHttpTraceRepository  : HTTP Trace: org.springframework.boot.actuate.trace.http.HttpTrace@29a5517a 
            
            curl http://localhost:8080/actuator -H "Authorization:Basic dXNlcjpwYXNzd29yZA=="
            curl http://localhost:8080/actuator/httptrace -H "Authorization:Basic dXNlcjpwYXNzd29yZA=="
            
    -- By web server access_log.log on log config
            

### UI list of all books
    http://localhost:8080/
    
### Public API resourse
    GET /api/books
    
    curl http://localhost:8080/api/books
    
### Restricted API resources
#### Usage
    Authentication: Basic, credentials: 
    
            user/password 

    curl -i -X <METHOD>  -d '<JSON payload>' \
    -H "Content-Type: application/json" \
    -H "Authorization:Basic dXNlcjpwYXNzd29yZA==" \
    http://localhost:8080/internal/<API-PATH>

* All over return date report
    GET /api/books/over/report
    curl http://localhost:8080/api/books/over/report \
    -H "Authorization:Basic dXNlcjpwYXNzd29yZA=="


* Find by tiltle part
    GET /api/books/find/%20NA
    curl http://localhost:8080/api/books/find/%20NA \ 
    -H "Authorization:Basic dXNlcjpwYXNzd29yZA=="


* Find user(borrower)
    GET /api/borrowers/find/{searchName}
    curl http://localhost:8080/api/borrowers/find/a \
    -H "Authorization:Basic dXNlcjpwYXNzd29yZA==" 


* Add user(borrower)
    POST /api/borrowers JSON:{"name":{borrowerName}}
    curl -i -X POST -d '{"name":"User ABC"}' http://localhost:8080/api/borrowers \
    -H "Content-Type: application/json" \
    -H "Authorization:Basic dXNlcjpwYXNzd29yZA=="
    
    
* Delete user(borrower)
    DELETE /api/borrowers JSON:{"name":{borrowerName}}
    curl -i -X DELETE  -d '{"name":"User ABC"}' http://localhost:8080/api/borrowers \
    -H "Content-Type: application/json" \
    -H "Authorization:Basic dXNlcjpwYXNzd29yZA==" 
    
    
* Register book loan
    POST /api/books/loan JSON:{"bookId": {bookId}, "borrowerName":{borrowerName}}
    curl -i -X POST -d '{"bookId": 4, "borrowerName":"User ABC"}' http://localhost:8080/api/books/loan \
    -H "Content-Type: application/json" \
    -H "Authorization:Basic dXNlcjpwYXNzd29yZA=="

* Return book (Unregister book loan)
    DELETE /api/books/loan JSON:{"bookId": {bookId}, "borrowerName":{borrowerName}}
    curl -i -X DELETE  -d '{"bookId": 4, "borrowerName":"User ABC"}' http://localhost:8080/api/books/loan \
    -H "Content-Type: application/json" \
    -H "Authorization:Basic dXNlcjpwYXNzd29yZA==" 
    
